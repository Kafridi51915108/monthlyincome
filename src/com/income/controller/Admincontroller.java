package com.income.controller;

import java.util.List;

import com.income.Admindao.AdminImpl;
import com.income.Admindao.Admin_Dao;
import com.income.model.Admin;
import com.income.model.emp_details;

public class Admincontroller {
	int result;
	Admin_Dao dao = new AdminImpl();

	public int adminAuthentication(String userid, String password) {

		Admin data = new Admin(userid, password);// for setting the value of an object

		return dao.adminAuthentication(data);

	}

	public List<emp_details> view_all() {
		return dao.view_all();
	}

	public int add_Details(int id, String name, String position, int projects_Done, int leaves_Taken, int working_Days,
			long mobile, long salary) {

		emp_details data = new emp_details(id, name, position, projects_Done, leaves_Taken, working_Days, mobile,
				salary);
		return dao.add_Details(data);
	}

	

	public int edit_Details1(int id,int projects_Done,int leaves_Taken,int working_Days) {
		emp_details data3 = new emp_details();
		
		data3.setId(id);
		data3.setProjects_Done(projects_Done);
		data3.setLeaves_Taken(leaves_Taken);
		data3.setWorking_Days(working_Days);
		
		return dao.edit_Details1(data3);
		
		
	}
	
	public int remove_Details(int id) {
		emp_details data2 = new emp_details();
		data2.setId(id);
		return dao.remove_Details(data2);
	}

}
