package com.income.Admindao;

import java.util.List;

import com.income.model.Admin;
import com.income.model.emp_details;


public interface Admin_Dao {

	public int adminAuthentication(Admin admin) ;
	
	public int add_Details(emp_details details);
	public List<emp_details>view_all();
	
	public int remove_Details(emp_details details);

	
	public int edit_Details1(emp_details details);
}
