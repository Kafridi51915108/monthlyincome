package com.income.Admindao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.income.model.Admin;
import com.income.model.emp_details;
import com.income.utill.Db;
import com.income.utill.Querry;

public class AdminImpl implements Admin_Dao {
	PreparedStatement pst;
	ResultSet rs;
	int result;

	@Override
	public int adminAuthentication(Admin admin) {
		result = 0;
		try {
			pst = Db.getConnection().prepareStatement(Querry.adminAuthentication);
			pst.setString(1, admin.getUserid());
			pst.setString(2, admin.getPassword());
			rs=pst.executeQuery();
			while (rs.next()) {
				result++;
			}
		} catch (ClassNotFoundException | SQLException e) {
					   System.out.println("Exception ocuurs in admin Authentication ");

		} finally {
			try {
				pst.close();
				rs.close();
				Db.getConnection().close();
			} catch (SQLException | ClassNotFoundException e) {

			}
		}
		return result;
	}

	@Override	//view all querry
	public List<emp_details> view_all() {
		List<emp_details> list = new ArrayList<emp_details>();
		try {
			pst = Db.getConnection().prepareStatement(Querry.view_all);

			rs = pst.executeQuery();
			while (rs.next()) {
				emp_details marks = new emp_details(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4),
						rs.getInt(5), rs.getInt(6), rs.getLong(7), rs.getLong(8));
				list.add(marks);
			}
		} catch (ClassNotFoundException | SQLException e) {
			System.out.println("Exception occurs in view_all details");
		} finally {

			try {
				rs.close();
				pst.close();
				Db.getConnection().close();
			} catch (ClassNotFoundException | SQLException e) {

			}
		}
		return list;

	}

	@Override	
	public int add_Details(emp_details details) {
		result = 0;
		try {
			pst = Db.getConnection().prepareStatement(Querry.add_Details);
			pst.setInt(1, details.getId());
			pst.setString(2, details.getName());
			pst.setString(3, details.getPosition());
			pst.setInt(4, details.getProjects_Done());
			pst.setInt(5, details.getLeaves_Taken());
			pst.setInt(6, details.getWorking_Days());
			pst.setLong(7, details.getMobile());
			pst.setLong(8, details.getSalary());

			result = pst.executeUpdate();
			
		} catch (ClassNotFoundException | SQLException e) {
			System.out.println("Exception occurs in add_Details");

		} finally { 
			try {
				pst.close();
				Db.getConnection().close();
			} catch (SQLException | ClassNotFoundException e) {

			}

		}
		return result;
	}


	

	@Override	//removing the data elements
	public int remove_Details(emp_details details) {
		result = 0;
		try {
			pst = Db.getConnection().prepareStatement(Querry.remove_Details);
			pst.setInt(1, details.getId());
			result = pst.executeUpdate();
		} catch (ClassNotFoundException | SQLException e) {
			System.out.println("Exception occurs in remove_Details");
		} finally {

			try {
				pst.close();
				Db.getConnection().close();
			} catch (ClassNotFoundException | SQLException e) {

			}
		}

		return result;

	}

	@Override
	public int edit_Details1(emp_details details) {
		result = 0;
		try {
			pst = Db.getConnection().prepareStatement(Querry.edit_Details1);
			
			pst.setInt(1, details.getProjects_Done());
			pst.setInt(2, details.getLeaves_Taken());
			pst.setInt(3, details.getWorking_Days());
			pst.setInt(4, details.getId());
			
			result = pst.executeUpdate();
		} catch (ClassNotFoundException | SQLException e) {

			System.out.println("Exception occurs in edit_Details2");
		} finally {

			try {
				pst.close();
				Db.getConnection().close();
			} catch (ClassNotFoundException | SQLException e) {

			}
		}

		return result;
		
	}
}
