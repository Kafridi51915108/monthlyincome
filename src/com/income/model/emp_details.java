package com.income.model;

public class emp_details {
	
	private int id;
	private String name;
	private String position;
	private int projects_Done;
	private int leaves_Taken;
	private int Working_Days;
	private Long mobile;
	private Long salary;
	
	public emp_details() {
		// TODO Auto-generated constructor stub

	}

	public emp_details(int id, String name, String position, int projects_Done, int leaves_Taken, int working_Days,
			long mobile, long salary) {
		super();
		this.id = id;
		this.name = name;
		this.position = position;
		this.projects_Done = projects_Done;
		this.leaves_Taken = leaves_Taken;
		this.Working_Days = working_Days;
		this.mobile = mobile;
		this.salary = salary;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public int getProjects_Done() {
		return projects_Done;
	}

	public void setProjects_Done(int projects_Done) {
		this.projects_Done = projects_Done;
	}

	public int getLeaves_Taken() {
		return leaves_Taken;
	}

	public void setLeaves_Taken(int leaves_Taken) {
		this.leaves_Taken = leaves_Taken;
	}

	public int getWorking_Days() {
		return Working_Days;
	}

	public void setWorking_Days(int working_Days) {
		Working_Days = working_Days;
	}

	public Long getMobile() {
		return mobile;
	}

	public void setMobile(Long mobile) {
		this.mobile = mobile;
	}

	public Long getSalary() {
		return salary;
	}

	public void setSalary(Long salary) {
		this.salary = salary;
	}
	
}
